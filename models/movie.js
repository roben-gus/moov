const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const URLSlugs = require('mongoose-url-slugs');
const tr = require('transliter');

const schema = new Schema(
    {
        kp_id: {
            type: String
        },
        name: {
            type: String,
            required: true
        },
        body: {
            type: String
        },
        year: {
            type: Number
        },
        image: {
            type: String
        },
        description: {
            type: String
        },
        countries: {
            type: String
        },
        rating: {
            type: String
        },
        actors: {
            type: String
        },
        directors: {
            type: String
        },
        genre: {
            type: String
        },
        age: {
            type: String
        },
        time: {
            type: String
        },
        trailer: {
            type: String
        },
        video: {
            type: String
        },
        url: {
            type: String
        },
        translator: {
            type: String
        },
        altname: {
            type: String
        },
        ranting: {
            type: String
        }

    },
    {
        timestamps: true
    }
);

schema.plugin(
    URLSlugs('title', {
        field: 'url',
        generator: text => tr.slugify(text)
})
);
schema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Movie', schema);
