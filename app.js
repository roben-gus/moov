const express = require('express');
const app = express();
const session = require('express-session');

const bodyParser = require('body-parser');
const path = require('path');
const staticAsset = require('static-asset');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const config = require('./config');
const routes = require('./routes');
var some = require('./publication.js');

const cron = require('node-cron');

// database
mongoose.Promise = global.Promise;
mongoose.connection
.on('error', error => console.log(error))
.on('close', () => console.log('Database connection closed.'))
.once('open', () => {
    const info = mongoose.connections[0];
console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
});
mongoose.connect(
    config.MONGO_URL,
    { useNewUrlParser: true}
);

//sessions
/*
app.use(session({
    secret: config.SESSION_SECRET,
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    })
})); */


app.use("/public", express.static(path.join(__dirname + "/public")));
app.use('/robots.txt', express.static(path.join(__dirname, '/public/robots.txt')));
app.use('/sitemap.txt', express.static(path.join(__dirname, '/public/sitemap.txt')));
app.set("/views", path.join(__dirname + "/views"));
app.set("view engine", "ejs");


//app.use("/public", express.static(__dirname + "/public"));
//app.set("view engine", "ejs");


app.use(bodyParser.urlencoded( { extended : true}));
app.use(bodyParser.json());
app.use('/', routes.film);
app.use('/contact', routes.contact);


app.use(function(req,res){
    res.status(404).render('404');
});

app.use(function(err, req, res, next) {
    return res.status(500).send({ error: err });
});

//task = cron.schedule('* * * * *', () => {
//    console.log('Printing this line every minute in the terminal');
//});

/* cron.schedule('* * * * *', () => {
    // code
    some();
}); */

// запуск сервера
app.listen(config.PORT, () =>
console.log(`Example app listening on port ${config.PORT}!`)
);
