const film = require('./film');
const contact = require('./contact');

module.exports = {
    film,
    contact
};