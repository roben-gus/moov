const express = require('express');
const router = express.Router();
//const moment = require('moment');
//moment.locale('ru');

const config = require('../config');
const models = require('../models');

async function films(req, res) {
    const perPage = +config.PER_PAGE;
    const page = req.params.page || 1;

    try {
        const films = await models.Movie.find({

        })
            .skip(perPage * page - perPage)
            .limit(perPage)
            .sort({ createdAt: -1 });

        const count = await models.Movie.count();

    //    console.log(count);

        res.render('index', {
            films,
            current: page,
            pages: Math.ceil(count / perPage),

        });
    } catch (error) {
        throw new Error('Server Error');
    }
}
// routes
router.get('/', (req, res) => films(req, res));
router.get('/page=:page', (req, res) => films(req, res));

router.get('/:film', async (req, res, next) => {
    const url = req.params.film.trim().replace(/ +(?= )/g, '');

if (!url) {
    const err = new Error('Not Found');
 //   err.status = 404;
 //   next(err);
  res.status(404).render('404');
} else {
    try {
        const film = await models.Movie.findOne({
            url
        });

        if (!film) {
            const err = new Error('Not Found');
        //    err.status = 404;
        //    next(err);
         res.status(404).render('404');
        } else {


        //    console.log(film);

            res.render('film', {
                film

            });
        }
    } catch (error) {
        throw new Error('Server Error');
    }
}
});

/*
router.get('film/contact', (req, res) =>{
    res.render('contact');
}) ;*/

/*
router.get('/', (req, res) => {
    res.render('user/index');
}); */




module.exports = router;